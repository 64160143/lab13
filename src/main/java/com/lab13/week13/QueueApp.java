package com.lab13.week13;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.*;

public class QueueApp extends JFrame {
    JTextField txtname;
    JLabel lblq;
    JLabel lblc;
    JButton btnAddq,btngetQ,btnclearQ;
    LinkedList<String> queue;

    public QueueApp(){
        super("Queue App");
        queue = new LinkedList<>();
        this.setSize(400,300);


        txtname = new JTextField();
        txtname.setBounds(30, 10,200, 20);
        txtname.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               AddQ();
                
            }});

        btnAddq = new JButton("Add Queue");
        btnAddq.setBounds(250,10,100,20);
        btnAddq.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               AddQ();
                
            }});

        btngetQ = new JButton("Get Queue");
        btngetQ.setBounds(250,40,100,20);
        btngetQ.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                getQ();
                
            }});

        btnclearQ = new JButton("Clear Queue");
        btnclearQ.setBounds(250,70,120,20);
        btnclearQ.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ClearQ();
                
            }});


        lblq = new JLabel("Empty");
        lblq.setBounds(30, 40,200,20);

        lblc = new JLabel("???");
        lblc.setHorizontalAlignment(JLabel.CENTER);
        lblc.setFont(new Font("Serif",Font.PLAIN, 50));
        lblc.setBounds(30,70,200,50);

        this.add(lblc);
        this.add(lblq);
        this.add(btnAddq);
        this.add(btngetQ);
        this.add(btnclearQ);
        this.add(txtname);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        showQ();
        this.setVisible(true);
    }

    public void getQ(){
        if(queue.isEmpty()){
            lblc.setText("???");
            return;
        }
        String name = queue.remove();
        lblc.setText(name);
        showQ();
         
    }


    public void showQ(){
        if(queue.isEmpty()){
            lblq.setText("Empty");
        } else {
            lblq.setText(queue.toString());
        }
        
    }

    public void AddQ(){
       String name = txtname.getText();
       if(name.equals("")){
           return;
       }
       queue.add(name);
       txtname.setText("");
       showQ();
    }

    public void ClearQ(){
        queue.clear();
        lblc.setText("?");
        txtname.setText("");
        showQ();
    }



    public static void main(String[] args) {
        QueueApp frame = new QueueApp();
    }
}


