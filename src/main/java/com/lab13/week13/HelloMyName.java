package com.lab13.week13;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class HelloMyName extends JFrame {
    JLabel lblName;
    JTextField txtName;
    JButton btn;
    JLabel lblhello;
    public HelloMyName(){
        super("Hello My Name");
        lblName = new JLabel("Name: ");
        lblName.setBounds(10,10,50,20);
        lblName.setHorizontalAlignment(JLabel.RIGHT);

        txtName = new JTextField();
        txtName.setBounds(70,10,200,20);


        btn = new JButton("Hello");
        btn.setBounds(30,40,250,20);
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String myname = txtName.getText();
               lblhello.setText("Hello "+ myname);
                
            }
            
        });

        lblhello = new JLabel("hello");
        lblhello.setHorizontalAlignment(JLabel.CENTER);
        lblhello.setBounds(30,70,250,20);
    


        this.add(txtName);
        this.add(lblName);
        this.add(lblhello);
        this.add(btn);
        this.setLayout(null);
        this.setSize(400,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName(); 
        
    }
    
}
